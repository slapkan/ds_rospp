#!/usr/bin/env python

import rosbag

import os
import scipy.io
import argparse

from rosbag_common import *

native_types = ['bool',
                'int8', 'uint8', 'char', 'byte', # Include deprecated aliases
                'int16', 'uint16',
                'int32', 'uint32',
                'int64', 'uint64',
                'float32', 'float64',
                'string',
                'time', 'duration']
"""
The recursion needs to know what primitive types to not recurse into.  A complete list,
including deprecated aliases for primitives, is included above.
"""

class MatConverter(object):
    """
    A class to convert a bag file to a matlab file.

    You probably want to just call convert(bagfilename, matfilename), which
    does the conversion directly.
    """
    def __init__(self):
        self._var_dict = {}
        self._bag = None
        self._msg_types = None
        self._msg_topics = None

    def convert(self, bagfilename, matfilename):
        self.open(bagfilename)
        self.parse()
        self.save(matfilename)

    def open(self, bagfilename):
        """
        Open a bagfile and prepare for parsing

        :param bagfilename: The path of the bag file to read
        :return: None
        """
        # Open the bag file
        self._bag = rosbag.Bag(bagfilename)
        self._msg_types, self._msg_topics = self._bag.get_type_and_topic_info()

        # Initialize our dictionary with empty variables
        sanitized_names = [topic_to_varname(x) for x in self._msg_topics.keys()]
        self._var_dict = dict(zip(sanitized_names, [None]*len(sanitized_names)))

    def save(self, matfilename):
        """
        Save the contents of this file as a .mat
        :param matfilename: The name of the matfile to write
        :return: None
        """
        print 'Saving matfile...'
        scipy.io.savemat(matfilename, self._var_dict, oned_as='row')

    def parse(self):
        """
        Iterate through the currently open bagfile and save everything to a matfile
        :return:
        """

        # First, count the total number of messages we're gonna parse
        total_msgs = 0
        for t in self._msg_topics.values():
            total_msgs += t.message_count

        print 'Parsing bag file...'
        bar = ProgressBar()
        bar.update(0)

        count = 0
        for topic, msg, tstamp in self._bag.read_messages():
            key = topic_to_varname(topic)
            if self._var_dict[key]:
                self._append_message(self._var_dict[key], msg, tstamp)
            else:
                self._var_dict[key] = self._append_message(None, msg, tstamp)

            count += 1

            # Update the progress bar
            bar.update(float(count) / float(total_msgs))

    def _append_message(self, pyvar, msg, tstamp):
        """
        Append a message to a python dictionary

        :param pyvar: The python dictionary holding this variable; updated in place
        :param msg: The ROS message to convert
        :param tstamp: The rosbag timestamp for this variable
        :return: None
        """

        if not pyvar:
            # NOTE: [[]]*5 does NOT do what you think it does.  You get a list that's
            # 5 copies of the SAME list, not 5 independent lists
            pyvar = dict(zip(msg.__slots__, [[] for x in range(len(msg.__slots__))]))
            if tstamp is not None:
                pyvar['rosbag_time'] = []

        for fieldname, fieldtype in zip(msg.__slots__, msg._slot_types):
            # Check if this is an array type, and get a clean variable type
            if '[' in fieldtype:
                fieldtype = fieldtype[:fieldtype.find('[')]
                is_array = True
            else:
                is_array = False

            # Special handling for time types
            if fieldtype == 'time' or fieldtype == 'duration':

                if is_array:
                    pyvar[fieldname].append([x.to_sec() for x in getattr(msg, fieldname)])
                else:
                    pyvar[fieldname].append(getattr(msg, fieldname).to_sec())

            # Just copy native types-- recursion won't do the right things
            elif fieldtype in native_types:
                # Don't need special array handling, because arrays are already python lists
                pyvar[fieldname].append(getattr(msg, fieldname))

            # It's not a time, and its not a primitive type.  So we have to recurse into it.
            else:
                if is_array:
                    pyvar[fieldname].append([self._append_message(None, x, None)
                                            for x in getattr(msg, fieldname)])
                else:
                    if not pyvar[fieldname]:
                        pyvar[fieldname] = self._append_message(None, getattr(msg, fieldname), None)
                    else:
                        self._append_message(pyvar[fieldname], getattr(msg, fieldname), None)

        # Finally, add the rosbag time
        if tstamp is not None:
            pyvar['rosbag_time'].append(tstamp.to_sec())

        return pyvar


def replace_ext(filename, new_ext):
    basename, ext = os.path.splitext(filename)
    return basename + '.' + new_ext


def rosbag2mat_main():
    parser = argparse.ArgumentParser(description="Convert a single rosbag to a matlab file")
    parser.add_argument('--output-path', '-o', default=None, help='Output Path')
    parser.add_argument('bag', nargs='+', help='Bagfile path(s)')

    args = parser.parse_args()
    converter = MatConverter()

    if args.output_path:
        outpath = args.output_path
    else:
        outpath = os.getcwd()

    for filename in args.bag:
        outfile = replace_ext(os.path.basename(filename), 'mat')
        outputname = os.path.join(outpath, outfile)
        print 'Converting \"%s\" to \"%s\"...' % (filename, outputname)

        converter.convert(filename, outputname)


if __name__ == '__main__':
    rosbag2mat_main()


