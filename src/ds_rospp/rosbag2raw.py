import rosbag

import os
import datetime
import codecs
import binascii
import argparse

from rosbag_common import *

# Sets the name of the message type representing raw data
raw_typename = 'ds_core_msgs/RawData'



class RawConverter(object):

    @staticmethod
    def read_index(bagfilename):
        index = []
        bag = rosbag.Bag(bagfilename)
        msg_types, msg_topics = bag.get_type_and_topic_info()
        for topicname, topicinfo in msg_topics.iteritems():
            if topicinfo.msg_type == raw_typename:
                index.append({'topic': topicname,
                              'count': topicinfo.message_count,
                              'connections': topicinfo.connections,
                              'frequency': topicinfo.frequency})

        return index

    @staticmethod
    def convert(bagfilename, outputname, topicname, encode_hex=False, encode_raw=False, hex_spacing=0):
        bag = rosbag.Bag(bagfilename)
        outfile = open(outputname, 'w')

        hex_spacing_int = int(hex_spacing)

        try:
            bar = ProgressBar()
            bar.update(0)
            msg_parsed = 0
            msg_count = bag.get_message_count(topicname)

            print 'Parsing %d messages from file \"%s\"\n' % (msg_count, bagfilename)

            for topic, msg, t in bag.read_messages(topics=[topicname]):
                if msg.data_direction == msg.DATA_IN:
                    tag = "DEV_2_HOST"
                elif msg.data_direction == msg.DATA_OUT:
                    tag = "HOST_2_DEV"
                else:
                    tag = "UNKNOWN_DIR"

                if encode_hex:
                    msgstr = binascii.hexlify(msg.data)
                    msglen = len(msg.data)
                    if hex_spacing_int > 0:
                        spacing = 2*hex_spacing_int
                        msgstr = ' '.join(splitEvery(msgstr, spacing))
                elif encode_raw:
                    # print(repr(msg.data))
                    # msgstr = ''.join([chr(x) for x in msg.data])
                    msgstr = msg.data
                else:
                    msgstr,msglen = codecs.escape_encode(msg.data)

                towrite = 'RAW\t%s\t%s\t%s\n' % (
                    datetime.datetime.utcfromtimestamp(t.to_sec()).isoformat(),
                    tag,
                    msgstr
                )
                #print towrite,
                outfile.write(towrite)

                msg_parsed += 1
                bar.update(float(msg_parsed) / float(msg_count))

        finally:
            outfile.close()
            bag.close()


def replace_ext(filename, new_ext):
    basename, ext = os.path.splitext(filename)
    return basename + '.' + new_ext


def rosbag2raw_main():
    parser = argparse.ArgumentParser(description="Extract raw serial data from a rosbag recored with ds_core")
    parser.add_argument('-x', '--hex', action='store_true', help='Extract input data as hex')
    parser.add_argument('-s', '--hex-spacing', default=0, help='Space hex characters every n bytes')
    parser.add_argument('-l', '--list', action='store_true',
                        help='List available raw topics in a file and immediately quit')
    parser.add_argument('-a', '--all', action='store_true', help="Convert all raw topics")
    parser.add_argument('-r', '--raw', action='store_true',
                        help='Do not escape special characters; just write the raw serial data exactly as it is.')
    parser.add_argument('-t', '--topic', help='Topics to extract.')
    parser.add_argument('bags', nargs='+', help='Bag files to parse')

    args = parser.parse_args()
    converter = RawConverter()

    baglist = args.bags

    # If we're in list mode, treat all positional arguments as bagfile names
    if args.list:
    	baglist.append(args.topic)
    
    if not args.list and not args.all and args.topic is None:
        print("Must supply a topic with '-t' or extract all with '-a'")
        return -1
       
    for filename in baglist:
        if filename is None:
            break
        print 'Opening bag file \"%s\"' % filename
        index = converter.read_index(filename)
        if args.list:
            print 'Index of file \"%s\"' % filename
            print '%50s %15s %5s %5s' % ('Topic', 'Messages', 'Conns', 'Freq.')
            for topic in index:
                print '%50s %15d %5d %5.2f' % (topic['topic'],
                                               topic['count'],
                                               topic['connections'],
                                               topic['frequency'])
            continue
        if args.all:
            for topic in index:
                basename = os.path.basename(filename)
                name, ext = os.path.splitext(basename)
                topic_cleaned = topic_to_varname(topic['topic'])
                outputname = os.path.join(os.getcwd(), name + '_' + topic_cleaned + '.DAT')
                print 'Converting \"%s\"' % filename
                print '        to \"%s\"' % outputname
                converter.convert(filename, outputname, topic['topic'], args.hex, args.raw, int(args.hex_spacing))
                
            
        else:
            basename = os.path.basename(filename)
            name, ext = os.path.splitext(basename)
            topic_cleaned = topic_to_varname(args.topic)
            outputname = os.path.join(os.getcwd(), name + '_' + topic_cleaned + '.DAT')
            print 'Converting \"%s\"' % filename
            print '        to \"%s\"' % outputname
            converter.convert(filename, outputname, args.topic,
                                  args.hex, args.raw, int(args.hex_spacing))


if __name__ == '__main__':
    rosbag2raw_main()
